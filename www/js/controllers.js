angular.module('rvtApp.controllers', ['rvtApp.services'])

.controller('rvtsCtrl', function($scope, rvtService, $ionicLoading) {

  $ionicLoading.show();

  rvtService.getRvts().then(function(rvts) {
    $scope.rvts = rvts;
  }).catch(function(response) {
    console.log("Error in controller");
  }).finally(function() {
    $ionicLoading.hide();
  });


  $scope.loadMore = function() {
    rvtService.getMoreRvts().then(function(rvts) {
      $scope.rvts = rvts;
    }).catch(function(response) {
      //request was not successfuls
      //handle the error
    }).finally(function() {
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });

  }

})

.controller('rvtDetailCtrl', function($scope, $stateParams, rvtService, $ionicLoading, $state) {

  $ionicLoading.show();

  rvtService.getRvt($stateParams.rvtId).then(function(rvt) {
    $scope.rvt = rvt;
    console.log( $scope.rvt);
  }).catch(function(response) {
    console.log("Error in controller");
  }).finally(function() {
    $ionicLoading.hide();
  });

  $scope.contactSeller = function() {
    $state.go('rvt-form', {
      rvtId: $scope.rvt.id
    });
  }

});