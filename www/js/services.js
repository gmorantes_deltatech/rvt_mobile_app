angular.module('rvtApp.services', [])

.factory('rvtService', function($http) {

  //var dataSource = 'http://iphone.rvt.com/index.php?format=json&option=com_rvt&version=1&view=rvs';
  var offset = 0;
  var limit = 12;
  var dataSource = 'http://iphone.rvt.com/index.php?format=json&option=com_rvt&version=1&view=rvs&offset=' + offset + '&limit=' + limit;
  var dataSourceDetail = 'http://iphone.rvt.com/index.php?option=com_rvt&format=json&view=rv&id=';
  var rvts = [];

  return {
    getRvts: function() {
      console.log(dataSource);
      return $http.get(dataSource).then(function(response) {
        rvts = response.data;
        return rvts;
      }, function(error) {
        console.log("Service error");
      });
    },

    getMoreRvts: function() {

      offset = offset + limit;
      dataSourceMore = 'http://iphone.rvt.com/index.php?format=json&option=com_rvt&version=1&view=rvs&offset=' + offset + '&limit=' + limit;
      console.log(dataSourceMore);
      return $http.get(dataSourceMore).then(function(response) {
        //rvts = response.data;
        rvts = rvts.concat(response.data);
        return rvts;
      }, function(error) {
        //something went wrong!
        //Optionally, we can just: return error;
      });
    },

    getRvt: function(id) {
      console.log(dataSourceDetail+ id);
      return $http.get(dataSourceDetail+ id).then(function(response) {
        rvt = response.data;
        console.log(rvt);
        return rvt;
      }, function(error) {
        //something went wrong!
        //Optionally, we can just: return error;
      });
      /*dataSourceDetail =
      for (i = 0; i < rvts.length; i++) {
        if (rvts[i].id == id) {
          return rvts[i];
        }
      }*/
    }
  };
});